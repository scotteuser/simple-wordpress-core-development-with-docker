# Simple docker development with WordPress Core git mirror.

This repository is a quick docker environment for testing WordPress Core
and testing plugins against the latest core. It uses the core git mirror.

Run the following to get started:

`make build`

You must have docker installed on your machine already.

URLs:

* http://wp.docker.localhost:8000/ - The WordPress site
* http://pma.wp.docker.localhost:8000/ - PhpMyAdmin
* http://mailhog.wp.docker.localhost:8000/ - MailHog

Thanks to Wodby for their docker-compose.yml which serves as a foundation.
