make build:
	# Clone the WordPress core mirror.
	git clone git://develop.git.wordpress.org/ web

	# Build the containers.
	docker-compose up -d

	# Start the containers.
	docker-compose Start

	####################################
	# Happy development!               #
	# http://wp.docker.localhost:8000/ #
	####################################
